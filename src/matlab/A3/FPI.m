function y = FPI(f, g, init_val, max_iter, TOL)
  y = init_val;
  display(y, "currently");
  hit_tol = 0;
  for i=1:max_iter
    y = g(y);
    display(y, "currently");
    if !(hit_tol) && abs(f(y)) < TOL
      printf("HIT TOL!\n")
      hit_tol = 1;
    endif
  endfor
endfunction