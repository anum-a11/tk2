function main()

##  documentation note:
##  all of these functions failed to diverge
##  g2 = @ (x) acos(x^2)/pi;
##  g2 = @ (x) 1/2 - (asin(x^2)/pi);

##  divergence
  g1 = @ (x) sqrt(cos(pi * x));
  g2 = @ (x) sqrt(sin((pi/2) - (pi * x)));
  g3 = @ (x) cos(pi * x)/x;
##  convergence
  g4 = @ (x) acos(x^2)/pi;
  g5 = @ (x) 1/2 - (asin(x^2)/pi);

  f  = @ (x) (x^2 - cos(pi * x));
  

  TOL = 10^-15;
  max_iter = 32;
  
  test_points = [-0.5, -0.4, 0.4, 0.5];
  for i=1:size(test_points, 2)
    DRYer(f, g1, test_points(i), max_iter, TOL, "g1"); 
  endfor
  for i=1:size(test_points, 2)
    DRYer(f, g2, test_points(i), max_iter, TOL, "g2"); 
  endfor
  for i=1:size(test_points, 2)
    DRYer(f, g3, test_points(i), max_iter, TOL, "g3"); 
  endfor
  
  for i=1:size(test_points, 2)
    DRYer(f, g4, test_points(i), max_iter, TOL, "g4"); 
  endfor
  
  for i=1:size(test_points, 2)
    DRYer(f, g5, test_points(i), max_iter, TOL, "g5"); 
  endfor
  
  for i=1:size(test_points, 2)
    DRYer2(f, test_points(i), max_iter, TOL, "steffensen"); 
  endfor
endfunction

function DRYer(f, g, init_val, max_iter, TOL, fun_name)
  printf("%s on %.2f\n", fun_name, init_val)
  FPI(f, g, init_val, max_iter, TOL, fun_name);
endfunction

function DRYer2(f, init_val, max_iter, TOL, fun_name)
  printf("%s on %.2f\n", fun_name, init_val)
  steffensen(f, init_val, max_iter, TOL, fun_name);
endfunction