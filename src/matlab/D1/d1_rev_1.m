% Dengan bantuan kelompok Muhammad Alif Saddid.
% docs: https://octave.org/doc/v4.2.0/Three_002dDimensional-Plots.html
tx = ty = linspace (0, -10);
[x, y] = meshgrid (tx, ty);
tz = sin(y) .* e.^((1 - cos(x)).^2)   + cos(x) .* e.^((1 - sin(y)).^2) + (x - y).^2;
mesh (tx, ty, tz);
xlabel('x')
ylabel('y')
zlabel('f(x, y)')
