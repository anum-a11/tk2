function main()
  ## f(x,y)=x^2-xy+y^2+e^xy
  ## g(w,x,y,z)=(w+1)(x+1)+(x+1)(y+1)+(y+1)(z+1)-w^2-x^2-y^2-z^2
  
  e = 2.71828;
  
  f = @(x, y) (x^2 - x*y + y^2 + e^(x*y));
  g = @(w,x,y,z) ((w+1)*(x+1)+(x+1)*(y+1)+(y+1)*(z+1)-w^2-x^2-y^2-z^2);
  
  dfx = @(x, y) (2*x - y + y * e^(x*y));
  dfy = @(x, y) (2*y - x + x * e^(x*y));
  
  dfxx = @(x, y) (2 + y^2 * e^(x*y));
  dfxy = @(x, y) (1 + (x*y + 1) * e^(x*y));
  dfyy = @(x, y) (2 + x^2 * e^(x*y));
  
  Hf = @(v) ( [ dfxx(v(1), v(2)),  dfxy(v(1), v(2))
                dfxy(v(1), v(2)), dfyy(v(1), v(2)) ]);
  
  dgw = @(w,x,y,z) (x+1-2*w);
  dgx = @(w,x,y,z) (w+1+y+1-2*x);
  dgy = @(w,x,y,z) (x+1+z+1-2*y);
  dgz = @(w,x,y,z) (y+1-2*z);
  
  dgww = -2; dgwx = 1; dgwy = 0; dgwz = 0;
  dgxx = -2; dgxy = 1; dgxz = 0;
  dgyy = -2; dgyz = 1;
  dgzz = -2;
  
  Hg = @(v) ([ dgww, dgwx, dgwy, dgwz
               dgwx, dgxx, dgxy, dgxz
               dgwy, dgxy, dgyy, dgyz
               dgwz, dgxz, dgyz, dgzz ]);
  
  
  nablaf = @(v) ([ dfx(v(1), v(2)); dfy(v(1), v(2)) ]);
  nablag = @(v) ([ dgw(v(1), v(2), v(3), v(4))
                   dgx(v(1), v(2), v(3), v(4))
                   dgy(v(1), v(2), v(3), v(4))
                   dgz(v(1), v(2), v(3), v(4)) ]);
  
  TOL = ones(1,2) * 2^-10;
  printf("---------------------f(2,1)---------------------\n");
  newton_method(f, nablaf, Hf, [2,1]', TOL);
  printf("---------------------f(2,0)---------------------\n");
  newton_method(f, nablaf, Hf, [2,0]', TOL);
  
  TOL = ones(1,4) * 2^-10;
  printf("--------------g(-10,-10, -20, -10)--------------\n");
  newton_method(g, nablag, Hg, [-10 -10 -20 -10]', TOL);
  printf("--------------g( 50, 20,  30,  40)--------------\n");
  newton_method(g, nablag, Hg, [50 20 30 40]', TOL);
  
endfunction