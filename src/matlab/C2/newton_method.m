function x = newton_method(f, nablaf, Hf, x0, TOL)
  
  for j=1:10
    if (abs(nablaf(x0)) < TOL)
      break;
    endif
    x0 = x0 + Hf(x0) \ (-1*nablaf(x0))
  endfor
  
endfunction