function main()
  ## f(x,y)=x^2-xy+y^2+e^xy
  ## g(w,x,y,z)=(w+1)(x+1)+(x+1)(y+1)+(y+1)(z+1)-w^2-x^2-y^2-z^2
  
  e = 2.71828;
  
  f = @(v) (v(1,1)^2 - v(1,1)*v(1,2) + v(1,2)^2 + e^(v(1,1)*v(1,2)));
  g = @(v) ((v(1,1)+1)*(v(1,2)+1)+(v(1,2)+1)*(v(1,3)+1)+(v(1,3)+1)*(v(1,4)+1)-v(1,1)^2-v(1,2)^2-v(1,3)^2-v(1,4)^2);
  
  dfx = @(v) (2*v(1,1) - v(1,2) + v(1,2) * e^(v(1,1)*v(1,2)));
  dfy = @(v) (2*v(1,2) - v(1,1) + v(1,1) * e^(v(1,1)*v(1,2)));
  
  dgw = @(v) (v(1,2)+1-2*v(1,1));
  dgx = @(v) (v(1,1)+1+v(1,3)+1-2*v(1,2));
  dgy = @(v) (v(1,2)+1+v(1,4)+1-2*v(1,3));
  dgz = @(v) (v(1,3)+1-2*v(1,4));
  
  nablaf = @(v) ([ dfx(v) dfy(v) ]);
  nablag = @(v) ([ dgw(v) dgx(v) dgy(v) dgz(v) ]);
  
  max_iter = 5;
  printf("=================f(x,y)=================\n");
  printf("########## Self tuned alpha ##########\n");
  printf("----v = (1,1)----\n");
  steepest_advance(f, nablaf, [1 1], -0.512, max_iter); 
  printf("----v = (2,0)----\n");
  steepest_advance(f, nablaf, [2 0], -0.512, max_iter); 
  printf("############ first alpha #############\n");
  steepest_advance(f, nablaf, [1 1], -1/3, max_iter);
  printf("### first alpha with different x y ###\n");
  steepest_advance(f, nablaf, [2 0], -0.250537, max_iter);
  printf("### auto update alpha using fminbnd ##\n");
  printf("----v = (1,1)----\n");
  opt_steepest_advance(f, nablaf, [1 1], -1, max_iter);
  printf("----v = (2,0)----\n");
  opt_steepest_advance(f, nablaf, [2 0], -1, max_iter);
  
  max_iter = 32;
  printf("===============g(w,x,y,z)===============\n");
  printf("########## Self tuned alpha ##########\n");
  printf("----v = (-10,-10,-20,-10)----\n");
  steepest_advance(g, nablag, [-10 -10 -20 -10], 0.5, max_iter);
  printf("----v = ( 50, 20, 30, 40)----\n");
  steepest_advance(g, nablag, [50 20 30 40], 0.5, max_iter);
  printf("############ first alpha #############\n");
  steepest_advance(g, nablag, [-10 -10 -20 -10], 0.30319, max_iter);
  printf("### first alpha with different x y ###\n");
  steepest_advance(g, nablag, [50 20 30 40], 0.37336, max_iter);
  printf("### auto update alpha using fminbnd ##\n");
  printf("----v = (-10,-10,-20,-10)----\n");
  opt_steepest_advance(g, nablag, [-10 -10 -20 -10], 1, max_iter);
  printf("----v = ( 50, 20, 30, 40)----\n");
  opt_steepest_advance(g, nablag, [50 20 30 40], 1, max_iter);
  
endfunction