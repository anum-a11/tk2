function x_i = opt_steepest_advance(f, nablaf, x0, dir, max_iter)
  
  nextnabla = nablaf(x0);
  hn = @(h) (f(x0 + nextnabla * h));
  [alpha, min] = fminbnd(@(a)(-1*dir*hn(a)), -1, 1);

  for i = 1:max_iter
    x0 = x0 + alpha * nextnabla;
    nextnabla = nablaf(x0);
    display(x0, "x");
    display(f(x0), "fx");
    if nextnabla == 0
      break;
    hn = @(h) (f(x0 + nextnabla * h));
    [alpha, min] = fminbnd(@(a)(-1*hn(a)), -1, 1);
    endif
  endfor
  display(x0, "x");
  display(f(x0), "fx");
  display(i, "iterations");
  disp("\n");
  x_i = x0;
endfunction