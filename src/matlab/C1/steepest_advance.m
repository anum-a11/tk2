function x_i = steepest_advance(f, nablaf, x0, alpha, max_iter)
  
  nextnabla = nablaf(x0);

  for i = 1:max_iter
    x0 = x0 + alpha * nextnabla;
    nextnabla = nablaf(x0);
    display(x0, "x");
    display(f(x0), "fx");
    if nextnabla == 0
      break;
    endif
  endfor
  display(x0, "x");
  display(f(x0), "fx");
  display(i, "iterations");
  disp("\n");
  x_i = x0;
endfunction