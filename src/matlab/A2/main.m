function main()
  
  TOL = 10^-15;
##  the bisection part
  printf("==========Bisection for equation 1==========\n");
  f = @(x) (x^2 - cos(pi * x));
  
  printf("==================1st root==================\n");
  bisection(f, 0, 0.5, TOL);
  printf("==================2nd root==================\n");
  bisection(f, -0.5, 0, TOL);
  
  f = @(x) (x^x - x - 7);
  printf("==========Bisection for equation 2==========\n");
  bisection(f, 1/e, 5, TOL)

  f = @(x) (x^7 + 6*x^6 + 2*x^5 - 35*x^4 - 19*x^3 + 72*x^2 - 27);
  printf("==========Bisection for equation 3==========\n");
  printf("==================1st root==================\n");  
  bisection(f, -3.1, -2.9, TOL);
  printf("==================2nd root==================\n");
  bisection(f, 0.9, 1.1, TOL);
  printf("==================3rd root==================\n");
  bisection(f, 1, 2, TOL);
  printf("==================4th root==================\n");
  bisection(f, -3, 1, TOL);

##  the newton part
  max_iter = 100;
  printf("========Newton-Raphson for equation 1========\n");
  f = @(x) (x^2 - cos(pi * x));
  df = @(x) (2 * x + pi * sin(pi * x));
  printf("==================1st root==================\n");
  newton_raphson(f, df, 0.1, max_iter, TOL);
  printf("==================2nd root==================\n");
  newton_raphson(f, df, -0.1, max_iter, TOL);

  printf("========Newton-Raphson for equation 2========\n");
  f = @(x) (x^x - x - 7);
  df = @(x) ((x^x) * (log(x) + 1)^2) + x^(x-1);
  newton_raphson(f, df, (1/e) + 0.01, max_iter, TOL);

  printf("========Newton-Raphson for equation 3========\n");
  f = @(x) (x^7 + 6*x^6 + 2*x^5 - 35*x^4 - 19*x^3 + 72*x^2 - 27);
  df = @(x) (7*x^6+36*x^5+10*x^4-140*x^3-57*x^2+144*x);
  printf("==================1st root==================\n");
  newton_raphson(f, df, -2, max_iter, TOL);
  printf("==================2nd root==================\n");
  newton_raphson(f, df, 0.5, max_iter, TOL);
  printf("==================3rd root==================\n");
  newton_raphson(f, df, -1, max_iter, TOL);
  printf("==================4th root==================\n");
  printf("####### Initial Guess ######\n");
  newton_raphson(f, df, 2, max_iter, TOL);
  printf("##### New Guess For A5 #####\n");
  newton_raphson(f, df, 1.5, max_iter, TOL);

endfunction