function result = bisection(func, left, right, TOL)  while (right - left) / 2 > TOL    result = (right + left) / 2;    display(result, "result")    if func(result) == 0      break;
    endif        if func(left) * func(result) < 0      right = result;    else      left = result;
    endif
  endwhile
endfunction
