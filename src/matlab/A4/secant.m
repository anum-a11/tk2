function x_n = secant(f, x0, x1, max_iter, TOL)
  x_n_1 = x0;
  x_n_2 = x1;
  
  hit_tol = 0;
  for i=1:max_iter
    fx_n_1 = f(x_n_1);
    fx_n_2 = f(x_n_2);
    x_n = ((x_n_2 * fx_n_1) - (x_n_1 * fx_n_2))/(fx_n_1-fx_n_2);
##    temp = x_n_1;
##    x_n_1 = x_n;
##    x_n_2 = temp;
    [x_n_1, x_n_2] = {x_n, x_n_1}{:};
    display([x_n, x_n_1, x_n_2]);
    if !(hit_tol) && (abs(f(x_n)) < TOL | abs(fx_n_1 - fx_n_2) < TOL)
        printf("HIT TOL!\n")
        hit_tol = 1;
    endif
  endfor
endfunction