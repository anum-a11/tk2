function main()
  f = @ (x) (x^x - x - 7);
  secant(f, 2, 3, 32, 10^-15);
endfunction