function eg_val = find_eigen_value(A, max_iter)
  
  [n n] = size(A);
  i = 0;
  for i=1:max_iter
    i += 1;
    [Q, R] = QR(A);
    A = R * Q;   
  endfor
  
  eg_val = ones(n, 1);
  for k=1:n
    eg_val(k) = A(k, k);
  endfor
  
  display(A, "Last_A");
  
endfunction