function R = createCompanionMatrix()
  # x^7+6x^6+2x^5-35x^4-19x^3+72x^2-27=0
  
  R = [   0   0   0   0   0   0  27
          1   0   0   0   0   0   0
          0   1   0   0   0   0 -72
          0   0   1   0   0   0  19
          0   0   0   1   0   0  35
          0   0   0   0   1   0  -2
          0   0   0   0   0   1  -6]
  
endfunction