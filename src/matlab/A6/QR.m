function [Q, R] = QR(A) 
  [m n] = size(A);
  Q = eye(m); 
  for k=1:n
    % cari kolom dengan norm terbesar 
    H = eye(m);
    for i=k:n 
      normv(i) = norm(A(k:m,i));
    endfor
    [maksi pos] = max(normv);
    pos = pos + k - 1;
    % Q' (A P) = R 
    % tukar kolom ke pos, dan kolom ke-k 
    % jangan lupa tukar vektor p 
    % Ambil dari baris k sampai m saja, pada kolom ke-k 
    e1 = zeros(m - k + 1, 1); 
    e1(1) = 1; 
    signOp = sign(A(k, k));
    if (signOp == 0)
      signOp = 1;
    endif
    v = A(k:m, k) + signOp * norm(A(k:m,k)) * e1;
    alpha = 2/(v' * v);
    for j=k:n
      A(k:m, j)= A(k:m, j) - alpha * v' * A(k:m, j) * v; 
      H(k:m, j)= H(k:m, j) - alpha * v' * H(k:m, j) * v; 
    endfor
    H2 = eye(m);
    for j=k:n
      for l=1:m
        H2(l, j)= Q(l,:) * H(:,j);
      endfor
    endfor

    Q(1:m,k:n) = H2(1:m,k:n);
  endfor
    R = A;
endfunction