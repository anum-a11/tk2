function At = main()
  A = createCompanionMatrix();
  true_eg_val = eig(A);
  
  disp(A);
  disp(true_eg_val);
  
  printf("###### 100 iterasi #####\n");
  eg_val = find_eigen_value(A, 100);
  eg_val = sort(eg_val, "descend");
  err = norm(eg_val - true_eg_val, 2);
  disp(eg_val);
  display(err, "err");
  printf("\n");
  
  printf("##### 1000 iterasi #####\n");
  eg_val = find_eigen_value(A, 1000);
  eg_val = sort(eg_val, "descend");
  err = norm(eg_val - true_eg_val, 2);
  disp(eg_val);
  display(err, "err");
  printf("\n");
  
  printf("#### 10000 iterasi #####\n");
  eg_val = find_eigen_value(A, 10000);
  eg_val = sort(eg_val, "descend");
  err = norm(eg_val - true_eg_val, 2);
  disp(eg_val);
  display(err, "err");
  printf("\n");
  
  printf("#### 100000 iterasi ####\n");
  eg_val = find_eigen_value(A, 100000);
  eg_val = sort(eg_val, "descend");
  err = norm(eg_val - true_eg_val, 2);
  disp(eg_val);
  display(err, "err");
  printf("\n");
  
endfunction