function y = inside_find(f, x, A, TOL)
  disp(x)
  while true && (!isnan(x))
    s = A(x) \ -f(x);
    x = x + s';
    disp(x)
    
    if (abs(f(x)) < TOL)
      break;
    endif
  endwhile
  display(x, "final_answer")
  y = x
endfunction