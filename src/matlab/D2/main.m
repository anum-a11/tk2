% Konsepsi metode dengan bantuan Muhammad Alif Saddid.
function main()
  
  TOL = ones(3,1) * 2^-40;
    
  mishra = @ (x,y) (sin(y) * exp((1-cos(x))^2) + cos(x) * exp((1-sin(y))^2) + (x-y)^2);
  
  # find at the border
  f1 = @ (x,y,l) (2 * (x - y) - sin(x) * e^((1 - sin(y))^2) + 2 * sin(x) * e^((1 - cos(x))^2) * (1 - cos(x)) * sin(y) - 2 * l *(x + 5));
  f2 = @ (x,y,l) (-2 * (x - y) + cos(y) * e^((1 - cos(x))^2) - 2 * cos(x) * e^((1 - sin(y))^2) * (1 - sin(y)) * cos(y) - 2 * l * (y + 5));
  f3 = @ (x,y,l) (25 - (x+5)^2 - (y+5)^2);
  
  f = @ (x) [f1(x(1), x(2), x(3)); f2(x(1), x(2), x(3)); f3(x(1), x(2), x(3))];
  
  f1x = @ (x,y,l)(4*sin(y)*e^(1-cos(x))^2*(1-cos(x))^2*sin(x)^2+2*sin(y)*e^(1-cos(x))^2*sin(x)^2+2*sin(y)*e^(1-cos(x))^2*(1-cos(x))*cos(x)-e^(1-sin(y))^2*cos(x)-2*l+2);
  f1y = @ (x,y,l)(2*sin(x)*e^(1-sin(y))^2*cos(y)*(1-sin(y))+2*e^(1-cos(x))^2*(1-cos(x))*sin(x)*cos(y)-2);
  f1l = @ (x,y,l)(-2*(x+5));

  f2x = @ (x,y,l)(2*cos(y)*e^(1-cos(x))^2*(1-cos(x))*sin(x)+2*e^(1-sin(y))^2*cos(y)*(1-sin(y))*sin(x)-2);
  f2y = @ (x,y,l)(2*cos(x)*e^(1-sin(y))^2*(1-sin(y))*sin(y)-e^(1-cos(x))^2*sin(y)+4*cos(x)*e^(1-sin(y))^2*cos(y)^2*(1-sin(y))^2+2*cos(x)*e^(1-sin(y))^2*cos(y)^2-2*l+2);
  f2l = @ (x,y,l)(-2*(y+5));
  
  f3x = @ (x,y,l) (-2*(x+5));
  f3y = @ (x,y,l) (-2*(y+5));
  f3l = @ (x,y,l) (0);

  J_three = @ (m) [
  f1x(m(1), m(2), m(3)), f1y(m(1), m(2), m(3)), f1l(m(1), m(2), m(3));
  f2x(m(1), m(2), m(3)), f2y(m(1), m(2), m(3)), f2l(m(1), m(2), m(3));
  f3x(m(1), m(2), m(3)), f3y(m(1), m(2), m(3)), f3l(m(1), m(2), m(3));
  ];
  
  A = J_three;
  
  printf("==================================== ON THE BORDER ====================================\n");
  result = dryer(f, [-10,-8,rand()], A, TOL);
  display(mishra(result(1), result(2)), "z");
  result = dryer(f, [-3,-8,rand()], A, TOL);
  display(mishra(result(1), result(2)), "z");
  result = dryer(f, [-3,-1,rand()], A, TOL);
  display(mishra(result(1), result(2)), "z");
  
  # find inside the border
  
  TOL = ones(2,1) * 2^-40;
  gy = @ (x,y) (-2*e^(1-sin(y))^2*cos(y)*(1-sin(y))*cos(x)+cos(y)*e^(1-cos(x))^2-2*(x-y));
  gx = @ (x,y) (2*sin(y)*e^(1-cos(x))^2*(1-cos(x))*sin(x)-e^(1-sin(y))^2*sin(x)+2*(x-y));
  
  gxx = @ (x,y) (4*sin(y)*e^(1-cos(x))^2*(1-cos(x))^2*sin(x)^2+2*sin(y)*e^(1-cos(x))^2*sin(x)^2+2*sin(y)*e^(1-cos(x))^2*(1-cos(x))*cos(x)-e^(1-sin(y))^2*cos(x)+2);
  gxy = @ (x,y) (2*sin(x)*e^(1-sin(y))^2*cos(y)*(1-sin(y))+2*e^(1-cos(x))^2*(1-cos(x))*sin(x)*cos(y)-2);
  
  gyx = @ (x,y)(2*cos(y)*e^(1-cos(x))^2*(1-cos(x))*sin(x)+2*e^(1-sin(y))^2*cos(y)*(1-sin(y))*sin(x)-2);
  gyy = @ (x,y)(2*cos(x)*e^(1-sin(y))^2*(1-sin(y))*sin(y)-e^(1-cos(x))^2*sin(y)+4*cos(x)*e^(1-sin(y))^2*cos(y)^2*(1-sin(y))^2+2*cos(x)*e^(1-sin(y))^2*cos(y)^2+2);
  
  g = @ (x) [gx(x(1), x(2)); gy(x(1), x(2))];
  
  J_two = @ (m) [
  gxx(m(1), m(2)), gxy(m(1), m(2));
  gyx(m(1), m(2)), gyy(m(1), m(2));
  ];
  
  A = J_two;
  
  printf("==================================== IN THE BORDER ====================================\n");
  result = dryer2(g, [-10,-8], A, TOL);
  display(mishra(result(1), result(2)), "z");
  result = dryer2(g, [-3,-8], A, TOL);
  display(mishra(result(1), result(2)), "z");
  result = dryer2(g, [-3,-1], A, TOL);
  display(mishra(result(1), result(2)), "z");
endfunction


function y = dryer(f, m, A, TOL)
  fprintf("==================================== FROM [ ")
  fprintf("%g ", m);
  fprintf("]====================================\n")
  y = border_find(f, m, A, TOL);
endfunction

function y = dryer2(f, m, A, TOL)
  fprintf("==================================== FROM [ ")
  fprintf("%g ", m);
  fprintf("]====================================\n")
  y = inside_find(f, m, A, TOL);
endfunction
