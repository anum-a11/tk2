function main()
  
  TOL = ones(3,1) * 2^-256;
  
  f1 = @ (u,v,w) (2 * u^2) - (4 * u) + (v^2) + (3 * v * w^2) + (6 * w) - 2;
  f2 = @ (u,v,w) ((u^2) * v) + (v^2) - (2 * v) + (2 * (w^2)) - 3;
  f3 = @ (u,v,w) (3 * (u^2)) - (12 * u) + (u * v^2) + (3 * w^2) + 2;
  
  f = @ (x) [f1(x(1), x(2), x(3)); f2(x(1), x(2), x(3)); f3(x(1), x(2), x(3))];
  
  f1u = @ (u,v,w) (4 * u) - 4;
  f1v = @ (u,v,w) (2 * v) + (3 * w^2);
  f1w = @ (u,v,w) (6 * v * w) + 6;

  f2u = @ (u,v,w) 2 * u * v;
  f2v = @ (u,v,w) u^2 + (2 * v) - 2;
  f2w = @ (u,v,w) 2 * w^2;

  f3u = @ (u,v,w) (6 * u) - 12 + v^2;
  f3v = @ (u,v,w) 2 * u * v;
  f3w = @ (u,v,w) 6 * w;
  
  J = @ (x) [
  f1u(x(1), x(2), x(3)), f1v(x(1), x(2), x(3)), f1w(x(1), x(2), x(3));
  f2u(x(1), x(2), x(3)), f2v(x(1), x(2), x(3)), f2w(x(1), x(2), x(3));
  f3u(x(1), x(2), x(3)), f3v(x(1), x(2), x(3)), f3w(x(1), x(2), x(3));
  ];
  
  x = [0.1,-0.1,0.1];
  disp(x)
  while true
##    display(abs(f(x)), "abs")
##    display(TOL, "tolerance")
##    display((abs(f(x)) > TOL), "while_loop")
##    display((inv(J(x)) * f(x)), "product_term")
##    display((x - (inv(J(x)) * f(x))'), "difference")
##    display(f(x), "fx")
    s = J(x) \ -f(x);
    x = x + s';
##    x = x - (inv(J(x)) * f(x))';
    
##    display(x, "x")
    disp(x);
    
    if (abs(f(x)) < TOL)
      break;
    endif
  endwhile
  
  display(x, "final_answer")
  f(x)

##  display(abs(f(x)), "abs")
##  display(TOL, "tolerance")
##  display((abs(f(x)) > TOL), "while_loop")
##  (abs(f(x)) > TOL) != [0, 0, 0]'
endfunction