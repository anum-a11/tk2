function x_i = modified_newton_raphson(f, df, df2, x0, max_iter, TOL)
  
  display(x0, "x0");
  hit_tol = 0;
  for i = 1:max_iter
    prev_x = x0;
    fx = f(x0); dfx = df(x0); df2x = df2(x0);
    x0 = x0 - (fx * dfx)/((dfx)^2 - fx * df2x);
    display(x0, "x");
    if !(hit_tol) && (abs(x0 - prev_x) < TOL)
      printf("HIT TOL!\n");
      printf("Iterations: %d\n", i);
      hit_tol = 1;
    endif
  endfor
  x_i = x0;
endfunction