function main()
  
  TOL = 10^-15;
  
  f = @(x) (x^7 + 6*x^6 + 2*x^5 - 35*x^4 - 19*x^3 + 72*x^2 - 27);
  df = @(x) (7*x^6+36*x^5+10*x^4-140*x^3-57*x^2+144*x);
  df2 = @(x) (42*x^5+180*x^4+40*x^3-420*x^2-114*x+144);
  
  max_iter = 100
  printf("==================1st root==================\n");
  modified_newton_raphson(f, df, df2, -2, max_iter, TOL);
  printf("==================2nd root==================\n");
  modified_newton_raphson(f, df, df2, 0.5, max_iter, TOL);
  printf("==================3rd root==================\n");
  modified_newton_raphson(f, df, df2, -1, max_iter, TOL);
  printf("==================4th root==================\n");
  printf("### Guess from A2 ###\n");
  modified_newton_raphson(f, df, df2, 2, max_iter, TOL);
  printf("##### New Guess #####\n");
  modified_newton_raphson(f, df, df2, 1.5, max_iter, TOL);
  
  
endfunction